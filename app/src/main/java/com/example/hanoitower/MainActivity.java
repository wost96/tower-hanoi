package com.example.hanoitower;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    LinearLayout towerOne, towerTwo, towerThree;
    ImageView ringOne, ringTwo, ringThree;
    TextView timerText, movesText;
    boolean gameStarted = false;
    int moveCounter = 0, timerCounter = 0;
    Timer timer;
    Handler timerHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.startButton).setOnClickListener(v -> {
            Button thisButton = (Button) v;
            // toggle the game state and do what needs to be done based
            // on that
            gameStarted = !gameStarted;
            if(gameStarted) {
                // game has started
                timer();
                thisButton.setText("STOP");
            } else {
                // game has been stopped
                resetGame();
                thisButton.setText("START");
            }
        });

        timerText = findViewById(R.id.timerText);
        movesText = findViewById(R.id.movesText);

        // Towers
        towerOne = findViewById(R.id.firstTower);
        towerTwo = findViewById(R.id.secondTower);
        towerThree = findViewById(R.id.thirdTower);

        towerOne.setOnDragListener(new MyDragListener());
        towerTwo.setOnDragListener(new MyDragListener());
        towerThree.setOnDragListener(new MyDragListener());

        // Rings
        ringOne = findViewById(R.id.ring_one);
        ringTwo = findViewById(R.id.ring_two);
        ringThree = findViewById(R.id.ring_three);

        ringOne.setOnTouchListener(new MyTouchListener());
        ringTwo.setOnTouchListener(new MyTouchListener());
        ringThree.setOnTouchListener(new MyTouchListener());
    }

    public void resetGame() {
        // Getting rid of all the views in the layouts,
        // removing the images inside.
        towerOne.removeAllViews();
        towerTwo.removeAllViews();
        towerThree.removeAllViews();

        // Re-adding the images to the first layout,
        // so we're back to how the game started.
        towerOne.addView(ringOne);
        towerOne.addView(ringTwo);
        towerOne.addView(ringThree);

        timerCounter = 0;
        moveCounter = 0;

        timer.cancel();
        timer.purge();
    }

    public void count() {
        moveCounter++;
        movesText.setText("Brukt: " + moveCounter + " trekk.");
    }

    public boolean hasWun() {
        if(towerThree.getChildCount() == 3) {
            return towerThree.getChildAt(0).equals(ringOne) &&
                    towerThree.getChildAt(1).equals(ringTwo) &&
                    towerThree.getChildAt(2).equals(ringThree);
        }
        return false;
    }

    private Runnable doUpdateTime = () -> {
        timerText.setText("Brukt: " + timerCounter + " sekunder.");
        timerCounter++;
    };

    public void timer() {
        timer = new Timer();
        try {
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    timerHandler.post(doUpdateTime);
                }
            }, 0, 1000);
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
        } catch (IllegalStateException ise) {
            ise.printStackTrace();
        }
    }

    private final class MyTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            LinearLayout owner = (LinearLayout) v.getParent();
            View top = owner.getChildAt(0);

            if (v == top) {
                if (gameStarted) {
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                    v.startDrag(data, shadowBuilder, v, 0);
                    v.setVisibility(View.INVISIBLE);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    class MyDragListener implements View.OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DROP:
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view);
                    LinearLayout container = (LinearLayout) v;
                    container.addView(view, 0);
                    view.setVisibility(View.VISIBLE);
                    count();
                    if(hasWun()) {
                        timer.cancel();
                        Toast.makeText(getApplicationContext(), "Du vant!", Toast.LENGTH_LONG)
                                .show();
                    }
                    break;
                default:
                    break;
            }
            return true;
        }
    }
}
